# Acuparse MQTT

A simple MQTT broker image for testing Acuparse MQTT messages.

The MQTT Broker server is launched with a default username and password of `acuparse` and has read/write access to the `acuparse` topic.

## Usage
```bash
docker run -d --rm --name mqtt -p 1883:1883 acuparse/mqtt
```

